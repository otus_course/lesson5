package ru.rt;

import java.util.ArrayList;

public class ArrayListFasade<T> implements IArray<T>{
    ArrayList<T> array = new ArrayList<>();

    @Override
    public int size() {
        return array.size();
    }

    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }

    @Override
    public T get(int index) {
        return array.get(index);
    }

    @Override
    public void put(T item) {
        array.add(item);
    }

    @Override
    public void put(T item, int index) {
        array.add(index, item);
    }

    @Override
    public T remove(int index) {
        return array.remove(index);
    }
}
