package ru.rt;

public class FactorArray<T> implements IArray<T>{
    private T[] array;
    private int size;

    public FactorArray() {
        this.size = 0;
        this.array = (T[])new Object[10];
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public void put(T item) {
        if(size() == array.length){
            plusSize();
        }
        array[size]=item;
        size++;
    }

    @Override
    public void put(T item, int index) {
        if(size() == array.length){
            plusSize();
        }
        for (int i = size()-1; i >= index; i--) {
            array[i+1]=array[i];
        }
        array[index]=item;
        size++;
    }

    @Override
    public T remove(int index) {
        T item = array[index];
        for (int i = index; i < size()-1; i++) {
            array[i]=array[i+1];
        }
        size--;
        //может и не нужно уменьшать размер массива
        if(size()/2 == array.length){
            minusSize();
        }
        return item;
    }

    private void minusSize() {
        T[] nArr = (T[])new Object[size()/2];
        if(!isEmpty()){
            System.arraycopy(array,0,nArr,0,size()/2);
        }
        array = nArr;
    }

    private void plusSize() {
        T[] nArr = (T[])new Object[size()*2];
        if(!isEmpty()){
            System.arraycopy(array,0,nArr,0,size());
        }
        array = nArr;
    }

    @Override
    public T get(int index) {
        return array[index];
    }
}
