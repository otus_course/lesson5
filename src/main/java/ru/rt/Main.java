package ru.rt;

public class Main {
    public static void main(String[] args) {
        IArray<Integer> array = new SpaceArray<>();


        array = new SpaceArray<>();
        testPutStart(array,100_000);

        array = new SpaceArray<>();
        testPutRandom(array,100_000);

        array = new SpaceArray<>();
        testPut(array,100_000);

        testGetStart(array,100_000);
        testGetRandom(array,100_000);
        testGetEnd(array,100_000);

        array = new SpaceArray<>();
        testRemoveFromStart(array,100_000);

        array = new SpaceArray<>();
        testRemoveRandom(array,100_000);

        array = new SpaceArray<>();
        testRemoveFromEnd(array,100_000);

    }

    public static void testPut(IArray<Integer> arr, int total){
        long start = System.currentTimeMillis();
        for (int i = 0; i < total; i++) {
            arr.put(i);
        }
        System.out.println(arr + " testPut:            " + (System.currentTimeMillis()-start));
    }

    public static void testGetStart(IArray<Integer> arr, int total){
        long start = System.currentTimeMillis();
        for (int i = 0; i < total; i++) {
            arr.get(0);
        }
        System.out.println(arr + " testGetStart:       " + (System.currentTimeMillis()-start));
    }

    public static void testGetRandom(IArray<Integer> arr, int total){
        long start = System.currentTimeMillis();
        for (int i = 0; i < total; i++) {
            arr.get((int)(Math.random() * arr.size()));
        }
        System.out.println(arr + " testGetRandom:       " + (System.currentTimeMillis()-start));
    }

    public static void testGetEnd(IArray<Integer> arr, int total){
        long start = System.currentTimeMillis();
        for (int i = 0; i < total; i++) {
            arr.get(arr.size()-1);
        }
        System.out.println(arr + " testGetEnd:          " + (System.currentTimeMillis()-start));
    }

    public static void testPutStart(IArray<Integer> arr, int total){
        long start = System.currentTimeMillis();
        for (int i = 0; i < total; i++) {
            arr.put(i,0);
        }
        System.out.println(arr + " testPutStart :       " + (System.currentTimeMillis()-start));
    }

    public static void testPutRandom(IArray<Integer> arr, int total){
        long start = System.currentTimeMillis();
        for (int i = 0; i < total; i++) {
            arr.put(i,(int)(Math.random() * arr.size()));
        }
        System.out.println(arr + " testPutRandom :      " + (System.currentTimeMillis()-start));
    }


    public static void testRemoveFromStart(IArray<Integer> arr, int total){
        for (int i = 0; i < total; i++) {
            arr.put(i);
        }
        long start = System.currentTimeMillis();
        while (arr.size()>0) {
            arr.remove(0);
        }
        System.out.println(arr + " testRemoveFromStart:  " + (System.currentTimeMillis()-start));
    }

    public static void testRemoveRandom(IArray<Integer> arr, int total){
        for (int i = 0; i < total; i++) {
            arr.put(i);
        }
        long start = System.currentTimeMillis();
        while (arr.size()>0) {
            arr.remove((int)(Math.random() * arr.size()));
        }
        System.out.println(arr + " testRemoveRandom:     " + (System.currentTimeMillis()-start));
    }

    public static void testRemoveFromEnd(IArray<Integer> arr, int total){
        for (int i = 0; i < total; i++) {
            arr.put(i);
        }
        long start = System.currentTimeMillis();
        while (arr.size()>0) {
            arr.remove(arr.size()-1);
        }
        System.out.println(arr + " testRemoveFromEnd:     " + (System.currentTimeMillis()-start));
    }
}
