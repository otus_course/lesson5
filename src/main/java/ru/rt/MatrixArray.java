package ru.rt;

public class MatrixArray <T> implements IArray<T>{
    protected IArray<T[]> array;
    protected int size;
    protected int vector;

    public MatrixArray() {
        this.size = 0;
        this.vector = 100;
        this.array = new FactorArray<>();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public void put(T item) {
        if(size() == array.size()*vector){
            array.put((T[])new Object[vector]);
        }
        array.get(size/vector)[size%vector] = item;
        size++;
    }

    @Override
    public void put(T item, int index) {
        if(size() == array.size()*vector){
            array.put((T[])new Object[vector]);
        }
        for (int i = size(); i >= index; i--) {
            array.get((index+1)/vector)[(index+1)%vector] = get(i);
        }

        array.get(index/vector)[index%vector] = item;
        size++;
    }

    @Override
    public T remove(int index) {
        T item = get(index);

        for (int i = index; i < size()-1; i++) {
            array.get(index/vector)[index%vector] = get(i+1);
        }
        if(array.get(array.size()-1).length==0){
            array.remove(size()-1);
        }
        size--;
        return item;
    }

    @Override
    public T get(int index) {
        return array.get(index/vector)[index%vector];
    }
}