package ru.rt;

public class SpaceArray<T> extends MatrixArray<T> {
    @Override
    public void put(T item) {
        if(size() == array.size()*(vector/2)){
            array.put((T[])new Object[vector]);
        }
        T[] ts = array.get(array.size() - 1);
        ts[ts.length-1] = item;
        size++;
    }

    @Override
    public void put(T item, int index) {
        if(size() == array.size()*(vector/2)){
            array.put((T[])new Object[vector]);
        }
        int realIndex=0;
        for (int arrayIndex = 0; arrayIndex < array.size() -1; arrayIndex++) {
            if(realIndex + array.get(arrayIndex).length-1 >= index){
                for (int i = (index - realIndex); i < array.get(arrayIndex).length-1; i++) {
                    array.get(arrayIndex)[i]=array.get(arrayIndex)[i+1];
                }
                array.get(arrayIndex)[(index - realIndex)] = item;
                if(array.get(arrayIndex).length == vector){
                    array.put((T[])new Object[vector],arrayIndex+1);
                    System.arraycopy(array.get(arrayIndex),vector/2,array.get(arrayIndex+1),0,vector/2);
                }
                break;
            }

            realIndex = realIndex + array.get(arrayIndex).length;
        }

        size++;
    }

    @Override
    public T remove(int index) {
        T t = get(index);
        int realIndex = 0;
        for (int arrayIndex = 0; arrayIndex < array.size()-1 ; arrayIndex++) {
            int arrL = array.get(arrayIndex).length;
            if(realIndex + arrL -1 >= index){
                if(arrL > 1){
                    for (int i = (index - realIndex); i < arrL -1; i++) {
                        array.get(arrayIndex)[i]=array.get(arrayIndex)[i+1];
                    }
                }else{
                    array.remove(arrayIndex);
                }
                break;
            }

            realIndex = realIndex + arrL;
        }
        size--;
        return t;
    }

    @Override
    public T get(int index) {
        int realIndex=0;
        for (int arrayIndex = 0; arrayIndex < array.size()-1 ; arrayIndex++) {
            if(realIndex + array.get(arrayIndex).length-1 >= index){
                return array.get(arrayIndex)[(index - realIndex)];
            }

            realIndex = realIndex + array.get(arrayIndex).length;
        }
        return null;
    }
}
