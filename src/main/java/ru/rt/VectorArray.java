package ru.rt;

public class VectorArray<T> implements IArray<T>{
    private T[] array;
    private int size;
    private int vector;

    public VectorArray(int vector) {
        this.vector = vector;
        this.array = (T[])new Object[vector];
        this.size = 0;
    }

    public VectorArray() {
        this(10);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public void put(T item) {
        if(size() == array.length){
            plusSize();
        }
        array[size]=item;
        size++;
    }

    @Override
    public void put(T item, int index) {
        if(size() == array.length){
            plusSize();
        }
        for (int i = size()-1; i >= index; i--) {
            array[i+1]=array[i];
        }
        array[index]=item;
        size++;
    }


    @Override
    public T remove(int index) {
        T item = array[index];
        for (int i = index; i < size()-1; i++) {
            array[i]=array[i+1];
        }
        size--;
        //может и не нужно уменьшать размер массива
        if(size()-vector == array.length){
            minusSize();
        }
        return item;
    }

    private void minusSize() {
        T[] nArr = (T[])new Object[size()-vector];
        if(!isEmpty()){
            System.arraycopy(array,0,nArr,0,size()-vector);
        }
        array = nArr;
    }

    private void plusSize() {
        T[] nArr = (T[])new Object[size()+vector];
        if(!isEmpty()){
            System.arraycopy(array,0,nArr,0,size());
        }
        array = nArr;
    }


    @Override
    public T get(int index) {
        return array[index];
    }
}
